package com.cancre.cancre.fragments.homefrags;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.cancre.cancre.Live_Stream.LiveMainActivity;
import com.cancre.cancre.javaClasses.commentsInterface;
import com.facebook.CallbackManager;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.cancre.cancre.R;
import com.cancre.cancre.activity.HashTagVideoActivity;
import com.cancre.cancre.activity.login_register.LoginActivity;
import com.cancre.cancre.activity.OtherUserProfileActivity;
import com.cancre.cancre.activity.login_register.RegisterActivity;
import com.cancre.cancre.activity.SoundVideoActivity;
import com.cancre.cancre.activity.videoEditor.util.Variables;
import com.cancre.cancre.adapters.AdapterVideoHome;
import com.cancre.cancre.javaClasses.FacebookLogin;
import com.cancre.cancre.javaClasses.GoogleLogin;
import com.cancre.cancre.javaClasses.ViewVideo;
import com.cancre.cancre.models.ModelVideoList;
import com.cancre.cancre.mvvm.VideoMvvm;
import com.cancre.cancre.utils.App;
import com.cancre.cancre.utils.AppConstants;
import com.cancre.cancre.utils.CommonUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import render.animations.Attention;
import render.animations.Render;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoHomeFragment extends Fragment implements View.OnClickListener {
    private View view;
    private RecyclerView recycler_video;
    private GoogleLogin googleLogin;
    private FacebookLogin facebookLogin;
    private CallbackManager callbackManager;
    private List<ModelVideoList.Detail> list = new ArrayList<>();
    private List<ModelVideoList.Detail> listNew = new ArrayList<>();
    VideoView videoView;
    private VideoMvvm videoMvvm;
    int currentPage = -1;
    LinearLayoutManager layoutManager;
    boolean is_visible_to_user = true;
    boolean is_user_stop_video = false;
    SpinKitView p_bar;
    ImageView img_loader;
    private RelativeLayout rl_no_internet;
    boolean isScrolling = false;
    int currentItems, totalItem, scrollOutItems, page = 0, followPage = 0;
    private AdapterVideoHome adapterVideoHome;
    private ViewVideo viewVideo;
    private TextView tv_noti_badge_count, tvCount, tv_hot_videos, tv_follow_videos, tv_live_videos;
    private RelativeLayout rl_not_badge;
    private ImageView img_logo_anime;
    // private List<GiftModel.Detail> listgift = new ArrayList<>();
    private VideoHotFragment videoHotFragment;
    private VideoFollowFragment videoFollowFragment;
    private  boolean back = false;

    public VideoHomeFragment() {
// Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
// Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_video_home, container, false);

        initids();


        if (!App.getSharedpref().isLogin(getActivity())) {
            tv_follow_videos.setVisibility(View.GONE);
        } else {

        }

        changeFrag(getVideoHotFragment());
// CacheUti

        OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {

                backPress();
            }
        };
        getActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(),onBackPressedCallback);
        return view;
    }

    private void backPress() {

//        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//        alertDialog.setTitle("Are you sure you want to exit?");
//        LayoutInflater layoutInflater = this.getLayoutInflater();
//        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                System.exit(0);
//            }
//        });
//        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.cancel();
//            }
//        });
//        alertDialog.show();


        if (back){
            System.exit(0);
        }
        else {
            back = true;
            Toast.makeText(getActivity(), "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    back = false;
                }
            }, 1500);
        }



    }

    private void initids() {
// tvCount = getActivity().findViewById(R.id.tvCount);
        rl_not_badge = getActivity().findViewById(R.id.rl_not_badge);
// tv_noti_badge_count = getActivity().findViewById(R.id.tvCount);
        tv_follow_videos = view.findViewById(R.id.tv_follow_videos);
        tv_hot_videos = view.findViewById(R.id.tv_hot_videos);
        img_logo_anime = view.findViewById(R.id.img_logo_anime);
        tv_live_videos = view.findViewById(R.id.tv_live_videos);

        tv_hot_videos.setOnClickListener(this);
        tv_follow_videos.setOnClickListener(this);
        tv_live_videos.setOnClickListener(this);
// Glide.with(getActivity()).asGif().load(R.raw.dots).into(img_loader);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_follow_videos:
                tv_follow_videos.setTextSize(18);
                tv_hot_videos.setTextSize(16);
                tv_live_videos.setTextSize(16);
                tv_follow_videos.setTextColor(getActivity().getColor(R.color.purple));
                tv_hot_videos.setTextColor(getActivity().getColor(R.color.grey));
                tv_live_videos.setTextColor(getActivity().getColor(R.color.grey));
                changeFrag(getFollowFragment());
                break;

            case R.id.tv_hot_videos:
                tv_follow_videos.setTextSize(16);
                tv_live_videos.setTextSize(16);
                tv_hot_videos.setTextSize(18);
                tv_follow_videos.setTextColor(getActivity().getColor(R.color.grey));
                tv_live_videos.setTextColor(getActivity().getColor(R.color.grey));
                tv_hot_videos.setTextColor(getActivity().getColor(R.color.purple));
                changeFrag(getVideoHotFragment());
                break;

            case R.id.tv_live_videos:
                tv_follow_videos.setTextSize(16);
                tv_hot_videos.setTextSize(16);
                tv_live_videos.setTextSize(18);
                tv_follow_videos.setTextColor(getActivity().getColor(R.color.grey));
                tv_hot_videos.setTextColor(getActivity().getColor(R.color.grey));
                tv_live_videos.setTextColor(getActivity().getColor(R.color.purple));
// changeFrag(getVideoHotFragment());
                startActivity(new Intent(getActivity(), LiveMainActivity.class));
//TODO live fragment
                break;


        }
    }

    private void changeFrag(Fragment fragment) {
        getChildFragmentManager().beginTransaction().replace(R.id.frag_container_video_home, fragment).commit();
    }

    private VideoFollowFragment getFollowFragment() {
        if (videoFollowFragment == null) {
            videoFollowFragment = new VideoFollowFragment();
        }
        return videoFollowFragment;
    }

    private VideoHotFragment getVideoHotFragment() {
        if (videoHotFragment == null) {
            videoHotFragment = new VideoHotFragment();
        }
        return videoHotFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        tv_follow_videos.setTextSize(16);
        tv_live_videos.setTextSize(16);
        tv_hot_videos.setTextSize(18);
        tv_follow_videos.setTextColor(getActivity().getColor(R.color.grey));
        tv_live_videos.setTextColor(getActivity().getColor(R.color.grey));
        tv_hot_videos.setTextColor(getActivity().getColor(R.color.purple));
        changeFrag(getVideoHotFragment());
    }

}
