package com.cancre.cancre.mvvm;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.cancre.cancre.models.ModelNotifications;
import com.cancre.cancre.retrofit.ApiClient;
import com.cancre.cancre.retrofit.ApiInterface;
import com.cancre.cancre.utils.CommonUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsMvvm extends ViewModel {

    private MutableLiveData<ModelNotifications> notificationsData;

    public LiveData<ModelNotifications> notifications(final Activity activity, String userid) {
        notificationsData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity,"Loading...");
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            apiInterface.getNotifications(userid).enqueue(new Callback<ModelNotifications>() {
                @Override
                public void onResponse(Call<ModelNotifications> call, Response<ModelNotifications> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        Log.i("notifications",response.body().toString());
                        notificationsData.postValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ModelNotifications> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    Log.i("notifications",t.getMessage());
                }
            });
        } else {
            Toast.makeText(activity, "No Internet", Toast.LENGTH_SHORT).show();
        }
        return notificationsData;
    }

}
