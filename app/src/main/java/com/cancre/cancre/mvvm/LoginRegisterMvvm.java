package com.cancre.cancre.mvvm;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.cancre.cancre.models.ModelLoginRegister;
import com.cancre.cancre.retrofit.ApiClient;
import com.cancre.cancre.retrofit.ApiInterface;
import com.cancre.cancre.utils.CommonUtils;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRegisterMvvm extends ViewModel {

    ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);


    //Check Email Phone
    private MutableLiveData<Map> phoneEmailData;

    public LiveData<Map> checkEmailPhone(final Activity activity, String Type, String PhoneEmail) {
        phoneEmailData = new MutableLiveData<>();

        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "Loading...");
            apiInterface.checkPhoneEmail(Type, PhoneEmail).enqueue(new Callback<Map>() {
                @Override
                public void onResponse(Call<Map> call, Response<Map> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        phoneEmailData.postValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<Map> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(activity, "No Internet!", Toast.LENGTH_SHORT).show();
        }
        return phoneEmailData;

    }


    //Register
    private MutableLiveData<ModelLoginRegister> registerdata;

    public LiveData<ModelLoginRegister> registerUser(final Activity activity, String type, String emailPhone, String password, String dob, String reg_id, String device_type) {

        registerdata = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "Loading...");
            apiInterface.userRegister(type, emailPhone, password, dob, reg_id, device_type).enqueue(new Callback<ModelLoginRegister>() {
                @Override
                public void onResponse(Call<ModelLoginRegister> call, Response<ModelLoginRegister> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        registerdata.postValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ModelLoginRegister> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(activity, "No Internet", Toast.LENGTH_SHORT).show();
        }
        return registerdata;
    }

    //Login
    private MutableLiveData<ModelLoginRegister> logindata;

    public LiveData<ModelLoginRegister> userLogin(final Activity activity, String emailPhone, String password, String reg_id, String deviceType) {
        logindata = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "Loading...");
            apiInterface.userlogin(emailPhone, password, reg_id, deviceType).enqueue(new Callback<ModelLoginRegister>() {
                @Override
                public void onResponse(Call<ModelLoginRegister> call, Response<ModelLoginRegister> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        logindata.postValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ModelLoginRegister> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(activity, "No Internet", Toast.LENGTH_SHORT).show();
        }

        return logindata;
    }

    //Social Login
    private MutableLiveData<ModelLoginRegister> socialLogindata;

    public LiveData<ModelLoginRegister> userSocialLogin(final Activity activity, String name, String socialId, String email, String phone, String regid, String image, String
            devicetype) {

        socialLogindata = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "Loading....");
            apiInterface.socialLogin(name, socialId, email, phone, regid, image, devicetype).enqueue(new Callback<ModelLoginRegister>() {
                @Override
                public void onResponse(Call<ModelLoginRegister> call, Response<ModelLoginRegister> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        socialLogindata.postValue(response.body());
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ModelLoginRegister> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    Log.e("socialLogin", t.getMessage());
                    Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(activity, "No Internet", Toast.LENGTH_SHORT).show();
        }
        return socialLogindata;

    }

    //logout
    private MutableLiveData<Map> logoutData;

    public LiveData<Map> logout(final Activity activity, String userId) {
        logoutData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            apiInterface.logout(userId).enqueue(new Callback<Map>() {
                @Override
                public void onResponse(Call<Map> call, Response<Map> response) {
                    CommonUtils.dismissProgress();
                    Log.i("logout", response.body().get("success").toString());
                    if (response.body() != null) {
                        logoutData.postValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<Map> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    Log.i("logout", t.getMessage());
                }
            });
        } else {
            Toast.makeText(activity, "No Internet", Toast.LENGTH_SHORT).show();
        }
        return logoutData;
    }

    //change password
    private MutableLiveData<ModelLoginRegister> changePasswordData;

    public LiveData<ModelLoginRegister> changePassword(final Activity activity, String useriD, String old_password, String new_password) {
        changePasswordData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "Loading...");
            apiInterface.changePassword(useriD, old_password, new_password).enqueue(new Callback<ModelLoginRegister>() {
                @Override
                public void onResponse(Call<ModelLoginRegister> call, Response<ModelLoginRegister> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        if (response.isSuccessful()) {
                            changePasswordData.postValue(response.body());
                            Log.i("changePassword", response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ModelLoginRegister> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    Log.i("changePassword", t.getMessage());
                }
            });
        } else {
            Toast.makeText(activity, "No Internet", Toast.LENGTH_SHORT).show();
        }
        return changePasswordData;
    }

    //otp forgot pass
    private MutableLiveData<Map> otpforgotPassdata;

    public LiveData<Map> otpforgotPass(final Activity activity, String type, String emailPhone) {
        otpforgotPassdata = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            otpforgotPassdata = new MutableLiveData<>();
            apiInterface.otpForgotPass(type, emailPhone).enqueue(new Callback<Map>() {
                @Override
                public void onResponse(Call<Map> call, Response<Map> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        if (response.body()!=null) {
//                            Log.i("otpForgotPass", response.body().get("otp").toString());
                            otpforgotPassdata.postValue(response.body());
                        }
                    }
                }

                @Override
                public void onFailure(Call<Map> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    Log.i("otpForgotPass", t.getMessage());
                }
            });
        } else {
            Toast.makeText(activity, "No Internet", Toast.LENGTH_SHORT).show();
        }
        return otpforgotPassdata;

    }

    private MutableLiveData<ModelLoginRegister> forgotPassData;

    public LiveData<ModelLoginRegister> forgotPass(final Activity activity, String type, String emailPhone, String password) {
        forgotPassData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "Loading...");
            apiInterface.forgotPass(type, emailPhone, password).enqueue(new Callback<ModelLoginRegister>() {
                @Override
                public void onResponse(Call<ModelLoginRegister> call, Response<ModelLoginRegister> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        if (response.isSuccessful()) {
                            Log.i("otpForgotPass", response.body().getMessage());
                            forgotPassData.postValue(response.body());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ModelLoginRegister> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    Log.i("otpForgotPass", t.getMessage());
                }
            });
        } else {
            Toast.makeText(activity, "No Internet", Toast.LENGTH_SHORT).show();
        }
        return forgotPassData;
    }

}
