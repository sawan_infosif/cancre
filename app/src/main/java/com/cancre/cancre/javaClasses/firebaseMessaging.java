package com.cancre.cancre.javaClasses;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.cancre.cancre.R;
import com.cancre.cancre.activity.HomeActivity;
import com.cancre.cancre.utils.App;
import com.cancre.cancre.utils.AppConstants;

public class firebaseMessaging extends FirebaseMessagingService {
    private final int NOTIFICATION_ID = 10;
    NotificationChannel channel = null;
    Uri defaultSound;
    Notification notification;
    NotificationChannel mChannel;

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        String type = remoteMessage.getData().get("type");

        if (type.equalsIgnoreCase(AppConstants.NOTI_FOLLOW)) {

            String loginId = remoteMessage.getData().get("loginId");
            String otherUserId = remoteMessage.getData().get("userId");

            App.getSingleton().setMyId(loginId);
            App.getSingleton().setOtherUserId(otherUserId);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                setBookingOreoNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), "",type);
            } else {
                showNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), "",type);
            }
        } else if (type.equalsIgnoreCase(AppConstants.NOTI_LIKE)) {

            String loginId = remoteMessage.getData().get("loginId");
            String otherUserId = remoteMessage.getData().get("userId");

            App.getSingleton().setMyId(loginId);
            App.getSingleton().setOtherUserId(otherUserId);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                setBookingOreoNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), "",type);
            } else {
                showNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), "",type);
            }
        }else if (type.equalsIgnoreCase(AppConstants.VIDEO)) {
            String loginId = remoteMessage.getData().get("loginId");
            String otherUserId = remoteMessage.getData().get("userId");

            App.getSingleton().setMyId(loginId);
            App.getSingleton().setOtherUserId(otherUserId);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                setBookingOreoNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), "",type);
            } else {
                showNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), "",type);
            }
        } else if (type.equalsIgnoreCase(AppConstants.COMMENT)) {
            String loginId = remoteMessage.getData().get("loginId");
            String otherUserId = remoteMessage.getData().get("userId");

            App.getSingleton().setMyId(loginId);
            App.getSingleton().setOtherUserId(otherUserId);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                setBookingOreoNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), "",type);
            } else {
                showNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), "",type);
            }
        }

    }

    private void setBookingOreoNotification(String title, String message, String s,String type) {

        Intent intent = new Intent(this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra("fragment",AppConstants.NOTIFICATION);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 101,
                intent, PendingIntent.FLAG_ONE_SHOT);

        defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

// Sets an ID for the notification, so it can be updated.
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = "CanCre";// The user-visible name of the channel.

        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }

        // Create a notification and set the notification channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notification = new Notification.Builder(this)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.logo)
                    .setSound(defaultSound)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setChannelId(CHANNEL_ID)
                    .build();
        }
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(this.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager.createNotificationChannel(mChannel);
        }

        // Issue the notification.
        mNotificationManager.notify(NOTIFICATION_ID, notification);
    }

    private void showNotification(String title, String message, String sx,String type) {
        Intent intent = new Intent(this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra("fragment",AppConstants.NOTIFICATION);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 101, intent, PendingIntent.FLAG_ONE_SHOT);
        defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        notification = new NotificationCompat.Builder(this)
                .setContentText(message)
                .setContentTitle(title)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.logo)
                .setAutoCancel(true)
                .setVibrate(new long[]{200, 200, 200, 200})
                .setSound(defaultSound)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notification);
    }
}
