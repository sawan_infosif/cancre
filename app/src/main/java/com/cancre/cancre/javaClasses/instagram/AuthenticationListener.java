package com.cancre.cancre.javaClasses.instagram;

public interface AuthenticationListener {
    void onTokenReceived(String auth_token);
}
