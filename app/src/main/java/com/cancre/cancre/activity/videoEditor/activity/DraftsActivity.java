package com.cancre.cancre.activity.videoEditor.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.cancre.cancre.R;
import com.cancre.cancre.activity.videoEditor.util.Variables;
import com.cancre.cancre.adapters.AdapterDrafts;
import com.cancre.cancre.models.ModelDrafts;
import com.cancre.cancre.utils.AppConstants;
import com.cancre.cancre.utils.CommonUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DraftsActivity extends AppCompatActivity {
    private RecyclerView rv_drafts;
    List<ModelDrafts> listPaths = new ArrayList<>();
    private AdapterDrafts adapterDrafts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonUtils.changeLanguage(DraftsActivity.this);
        setContentView(R.layout.activity_drafts);
        findIds();
        setRecyler();
    }

    private void setRecyler() {
        final List<File> list = getListFiles(new File(Variables.draft_app_folder));
        for (int i = 0; i < list.size(); i++) {
            ModelDrafts modelDrafts = new ModelDrafts();
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, ''yy hh:mm a");
            Date lastModDate = new Date(list.get(i).lastModified());
            String path = list.get(i).getPath();
            modelDrafts.setFilePath(path);
            modelDrafts.setFileDate(dateFormat.format(lastModDate));
            listPaths.add(modelDrafts);
        }
        adapterDrafts = new AdapterDrafts(DraftsActivity.this, listPaths, new AdapterDrafts.Select() {
            @Override
            public void post(int position, String description) {
                startActivity(new Intent(DraftsActivity.this, PostVideoActivity.class)
                        .putExtra(AppConstants.VIDEO_PATH, list.get(position).getPath())
                        .putExtra(AppConstants.VIDEO_DESCRIPTION, description));
            }

            @Override
            public void deletDrafts(int position) {
                deletDialog(position, list);
            }
        });
        rv_drafts.setAdapter(adapterDrafts);
    }

    private void deleteDraft(int position, List<File> fileList) {
        File removeFile = new File(fileList.get(position).getPath());
        listPaths.remove(position);
        adapterDrafts.notifyDataSetChanged();
        removeFile.delete();
        Toast.makeText(DraftsActivity.this, "Draft deleted!", Toast.LENGTH_SHORT).show();
    }

    private void deletDialog(final int position, final List<File> fileList) {
        final AlertDialog.Builder al = new AlertDialog.Builder(this, R.style.dialogStyle);
        al.setTitle("Delete Draft ?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteDraft(position, fileList);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setMessage("Do you want to delete the draft ?").show();
    }

    private void findIds() {
        rv_drafts = findViewById(R.id.rv_drafts);
    }

    //get files from drafts folder
    public static ArrayList<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files;
        files = parentDir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.getName().endsWith(".mp4")) {
                    if (!inFiles.contains(file)) inFiles.add(file);
                    Log.i("draftsFile", file.toString());
                }
            }
        }
        return inFiles;
    }

    public void backPress(View view) {
        onBackPressed();
    }
}


