package com.cancre.cancre.activity.videoEditor.network;

public interface DownloadAsyncResponse {
    void processFinish(boolean result);
}
