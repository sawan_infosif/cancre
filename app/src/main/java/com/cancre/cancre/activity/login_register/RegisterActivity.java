package com.cancre.cancre.activity.login_register;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.cancre.cancre.R;
import com.cancre.cancre.mvvm.LoginRegisterMvvm;
import com.cancre.cancre.utils.App;
import com.cancre.cancre.utils.AppConstants;
import com.cancre.cancre.utils.CommonUtils;

import java.util.Map;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private TextInputLayout input_email, input_phone;
    private RelativeLayout rl_email, rl_phone;
    private TextInputEditText et_phone, et_email, et_pass;
    private LoginRegisterMvvm loginRegisterMvvm;
    private String registerType = "email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonUtils.changeLanguage(RegisterActivity.this);
//        overridePendingTransition(R.anim.in_from_right, R.anim.in_from_left);
        setContentView(R.layout.activity_register);

        loginRegisterMvvm = ViewModelProviders.of(RegisterActivity.this).get(LoginRegisterMvvm.class);
        initIds();
    }

    private void initIds() {
        findViewById(R.id.tv_signin_reg).setOnClickListener(this);
        findViewById(R.id.btn_send_otp).setOnClickListener(this);
        findViewById(R.id.img_back_reg).setOnClickListener(this);
        et_pass = findViewById(R.id.et_pass);
        et_email = findViewById(R.id.et_email);
        et_phone = findViewById(R.id.et_phone);
        input_phone = findViewById(R.id.input_phone);
        input_email = findViewById(R.id.input_email);
        rl_phone = findViewById(R.id.rl_phone);
        rl_email = findViewById(R.id.rl_email);
        input_email = findViewById(R.id.input_email);
        input_phone = findViewById(R.id.input_phone);

        rl_email.setOnClickListener(this);
        rl_phone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_signin_reg:
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                break;

            case R.id.btn_send_otp:

                checkEmailPhone();
                break;

            case R.id.img_back_reg:
                onBackPressed();
                break;

            case R.id.rl_phone:
                registerType = "phone";
                rl_phone.setBackgroundColor(getResources().getColor(R.color.trans));
                rl_email.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                input_phone.setVisibility(View.VISIBLE);
                input_email.setVisibility(View.GONE);
                break;

            case R.id.rl_email:
                registerType = "email";
                rl_email.setBackgroundColor(getResources().getColor(R.color.trans));
                rl_phone.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                input_email.setVisibility(View.VISIBLE);
                input_phone.setVisibility(View.GONE);
                break;
        }
    }

    private void checkEmailPhone() {

        String emailPhone = "";
        String password = et_pass.getText().toString().trim();

        if (password.length() < 5) {
            et_pass.setError("Password cannot be less than 5 digits.");
            return;
        }

        if (registerType.equalsIgnoreCase("phone")) {
            emailPhone = et_phone.getText().toString();
            if (emailPhone.isEmpty()) {
                et_phone.setError("Phone Number Cannot Be Empty!");
                return;
            }
        } else {
            emailPhone = et_email.getText().toString();
            if (emailPhone.isEmpty()) {
                et_email.setError("Email Cannot Be Empty!");
                return;
            } else if (!Pattern.matches(Patterns.EMAIL_ADDRESS.pattern(), emailPhone)) {
                et_email.setError("Invalid Email");
                return;
            }
        }

        if (password.isEmpty()) {
            et_pass.setError("Password Cannot Be Empty!");
            return;
        }

        App.getSingleton().setEmailPhone(emailPhone);
        App.getSingleton().setRegisterType(registerType);
        App.getSingleton().setPassword(password);

        loginRegisterMvvm.checkEmailPhone(RegisterActivity.this, registerType, emailPhone).observe(RegisterActivity.this, new Observer<Map>() {
            @Override
            public void onChanged(Map map) {
                if (map.get("success").equals("1")) {

                    String otp = map.get("otp").toString();

                    startActivity(new Intent(RegisterActivity.this, VerifyOTPActivity.class).putExtra(AppConstants.OTP_KEY, otp));
                } else {
                    Toast.makeText(RegisterActivity.this, map.get("message").toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

