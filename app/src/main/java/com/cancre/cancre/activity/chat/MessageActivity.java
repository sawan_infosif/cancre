package com.cancre.cancre.activity.chat;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.cancre.cancre.R;
import com.cancre.cancre.activity.OtherUserProfileActivity;
import com.cancre.cancre.adapters.AdapterMessages;
import com.cancre.cancre.models.ModelInbox;
import com.cancre.cancre.models.ModelSendMessage;
import com.cancre.cancre.mvvm.MessageMvvm;
import com.cancre.cancre.utils.AppConstants;
import com.cancre.cancre.utils.CommonUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MessageActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView rv_message;
    private String receiverId, message, senderId, otherUserName, path, messageType;
    private EditText messageET;
    private String imagepath = "";
    private TextView chatUserNameTV, noConversationFoundTV;
    private ImageView sendIV, imageIV;
    private MessageMvvm messageMvvm;
    private List<ModelSendMessage.Detail> list = new ArrayList<>();
    private List<ModelSendMessage.Detail> newList = new ArrayList<>();
    private AdapterMessages adapterMessages;
    final Handler handler = new Handler();
    private String userId = CommonUtils.userId(MessageActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonUtils.changeLanguage(MessageActivity.this);
        setContentView(R.layout.activity_message);
        messageMvvm = ViewModelProviders.of(MessageActivity.this).get(MessageMvvm.class);
        receiverId = getIntent().getExtras().getString(AppConstants.OTHER_USER_ID);
        otherUserName = getIntent().getExtras().getString(AppConstants.OTHER_USER_NAME);
        path = getIntent().getExtras().getString(AppConstants.OTHER_USER_IMAGE);
        senderId = userId;
        Log.i("receiverId", receiverId);
        findIds();
        chatUserNameTV.setText(otherUserName);
        getConversationAPI();
        handler.postDelayed(runnable, 5000);
    }

    private void getConversationAPI() {
        messageMvvm.getConversation(this, senderId, receiverId).observe(this, new Observer<ModelSendMessage>() {
            @Override
            public void onChanged(ModelSendMessage modelSendMessage) {
                if (modelSendMessage.getSuccess().equalsIgnoreCase("1")) {
                    if (list.size() > 0) {
                        for (int i = list.size() - 1; i < modelSendMessage.getDetails().size(); i++) {
                            if (modelSendMessage.getDetails().get(i).getSenderId().equalsIgnoreCase(userId)) {

                            } else {
                                list.add(modelSendMessage.getDetails().get(i));
                            }
                        }
                    } else {
                        if (list.size() == modelSendMessage.getDetails().size()) {

                        } else {
                            list = modelSendMessage.getDetails();
                        }
                    }
                } else {
                    noConversationFoundTV.setVisibility(View.VISIBLE);
                }
                adapterMessages = new AdapterMessages(MessageActivity.this, list, path, new AdapterMessages.Select() {
                    @Override
                    public void deleteMessage(int position, String messageId, View view) {
                        deleteMessageMenu(position, view, messageId);
                    }

                    @Override
                    public void moveToProfile(int position) {
                        Intent intent=new Intent(MessageActivity.this, OtherUserProfileActivity.class);
                        String otherUserid=list.get(position).getReciverId();
                        if (list.get(position).getReciverId().equalsIgnoreCase(userId)) {
                            otherUserid=list.get(position).getSenderId();
                        }
                        intent.putExtra(AppConstants.OTHER_USER_ID,otherUserid);
                        startActivity(intent);
                    }
                });
                rv_message.setAdapter(adapterMessages);
                adapterMessages.notifyDataSetChanged();
                rv_message.scrollToPosition(list.size() - 1);
            }
        });
    }

    private void deleteMessageApi(final int position, String messageId) {

        messageMvvm.deleteMessage(MessageActivity.this, userId, messageId).observe(MessageActivity.this, new Observer<ModelInbox>() {
            @Override
            public void onChanged(ModelInbox modelInbox) {
                if (modelInbox.getSuccess().equalsIgnoreCase("1")) {
                    list.remove(position);
                    adapterMessages.notifyDataSetChanged();
                    Log.i("conversationData", modelInbox.getMessage());
                } else {
                    Log.i("conversationData", modelInbox.getMessage());
                }
            }
        });
    }

    private void findIds() {
        messageET = findViewById(R.id.messageET);
        chatUserNameTV = findViewById(R.id.chatUserNameTV);
        rv_message = findViewById(R.id.rv_message);
        noConversationFoundTV = findViewById(R.id.noConversationFoundTV);
        sendIV = findViewById(R.id.sendIV);
        imageIV = findViewById(R.id.imageIV);
        sendIV.setOnClickListener(this);
        imageIV.setOnClickListener(this);
    }

    private void validate() {
        message = messageET.getText().toString().trim();
        if (message.isEmpty()) {
            Toast.makeText(this, "Please enter message", Toast.LENGTH_SHORT).show();
        } else {
            senMessageApi("1");
        }
    }

    private void senMessageApi(String messageType) {
        RequestBody rb_senderId = RequestBody.create(MediaType.parse("text/plain"), senderId);
        RequestBody rb_receiverId = RequestBody.create(MediaType.parse("text/plain"), receiverId);
        RequestBody rb_message = RequestBody.create(MediaType.parse("text/plain"), message);
        RequestBody rb_messageType = RequestBody.create(MediaType.parse("text/plain"), messageType);
        MultipartBody.Part rb_image;
        // Toast.makeText(this, imagepath, Toast.LENGTH_SHORT).show();
        if (!imagepath.isEmpty()) {
            File file = new File(imagepath);
            final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            rb_image = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        } else {
            final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            rb_image = MultipartBody.Part.createFormData("image", "", requestFile);
        }
        messageMvvm.sendMessage(this, rb_senderId, rb_receiverId, rb_message, rb_messageType, rb_image).observe(this, new Observer<ModelSendMessage>() {
            @Override
            public void onChanged(ModelSendMessage modelSendMessage) {
                if (modelSendMessage.getSuccess().equalsIgnoreCase("1")) {
                    // Toast.makeText(MessageActivity.this, modelSendMessage.getMessage(), Toast.LENGTH_SHORT).show();
                    messageET.setText("");
                    list.add(modelSendMessage.getDetails().get(0));
                    noConversationFoundTV.setVisibility(View.GONE);
                    adapterMessages.notifyDataSetChanged();
                    rv_message.scrollToPosition(list.size() - 1);
                } else {
                     Toast.makeText(MessageActivity.this, modelSendMessage.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void backPress(View view) {
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sendIV:
                messageType = "1";
                validate();
                break;

            case R.id.imageIV:
                CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).start(this);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            //image
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    imagepath = resultUri.getPath();
                    // Glide.with(MessageActivity.this).load(imagepath).into(img_photo);
                    if (!imagepath.equalsIgnoreCase("")) {
                        messageType = "2";
                        senMessageApi("2");
                    } else {
                        messageType = "1";
                    }
                } else {
                    Toast.makeText(this, "Try Again", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            getConversationAPI();
            handler.postDelayed(this, 5000);
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(runnable);
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

    //TODO replace with popupWIndow
    private void deleteMessageMenu(final int position, View view, final String messageId) {
        PopupMenu popupMenu = new PopupMenu(MessageActivity.this, view);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.menu_delete_comment, popupMenu.getMenu());
        MenuItem tv_delete = popupMenu.getMenu().findItem(R.id.delete_comment);
        tv_delete.setTitle(R.string.delete_message);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete_comment:
                        deleteMessageApi(position, messageId);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }
}