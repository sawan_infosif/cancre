package com.cancre.cancre.activity.privacysettings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.cancre.cancre.R;
import com.cancre.cancre.adapters.AdapterBalance;
import com.cancre.cancre.utils.CommonUtils;

public class BalanceActivity extends AppCompatActivity {
    private RecyclerView rv_balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonUtils.changeLanguage(BalanceActivity.this);
        setContentView(R.layout.activity_balance);

        rv_balance = findViewById(R.id.rv_balance);
        rv_balance.setAdapter(new AdapterBalance(BalanceActivity.this));
    }

    public void back(View view) {
        onBackPressed();
    }
}
