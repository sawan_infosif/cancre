package com.cancre.cancre.activity.login_register;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cancre.cancre.R;
import com.cancre.cancre.mvvm.LoginRegisterMvvm;
import com.cancre.cancre.utils.App;
import com.cancre.cancre.utils.AppConstants;
import com.cancre.cancre.utils.CommonUtils;

import java.util.Map;

import render.animations.Attention;
import render.animations.Render;

public class VerifyOTPActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText et_otp1, et_otp2, et_otp3, et_otp4;
    private String otp;
    private LoginRegisterMvvm loginRegisterMvvm;
    private LinearLayout ll_otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonUtils.changeLanguage(VerifyOTPActivity.this);
        setContentView(R.layout.activity_verify_o_t_p);

        initIds();
        textwatcher();
        loginRegisterMvvm = ViewModelProviders.of(VerifyOTPActivity.this).get(LoginRegisterMvvm.class);
        otp = getIntent().getExtras().getString(AppConstants.OTP_KEY);
//        Toast.makeText(this, otp, Toast.LENGTH_SHORT).show();
    }

    private void initIds() {
        ll_otp = findViewById(R.id.ll_otp);
        et_otp1 = findViewById(R.id.et_otp1);
        et_otp2 = findViewById(R.id.et_otp2);
        et_otp3 = findViewById(R.id.et_otp3);
        et_otp4 = findViewById(R.id.et_otp4);
        findViewById(R.id.btn_done_otp).setOnClickListener(this);
        findViewById(R.id.img_back_otp).setOnClickListener(this);
        findViewById(R.id.tv_resend_otp).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_done_otp:
                matchOTP();
                break;

            case R.id.img_back_otp:
                onBackPressed();
                break;

            case R.id.tv_resend_otp:
                resendOTP();
                break;
        }
    }

    private void resendOTP() {
        String emailPhone = App.getSingleton().getEmailPhone();
        String type = App.getSingleton().getRegisterType();
        loginRegisterMvvm.checkEmailPhone(VerifyOTPActivity.this, type, emailPhone).observe(VerifyOTPActivity.this, new Observer<Map>() {
            @Override
            public void onChanged(Map map) {
                if (map.get("success").equals("1")) {

                    otp = map.get("otp").toString();
//                    Toast.makeText(VerifyOTPActivity.this, otp, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(VerifyOTPActivity.this, map.get("message").toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void matchOTP() {
        String otp1 = et_otp1.getText().toString();
        String otp2 = et_otp2.getText().toString();
        String otp3 = et_otp3.getText().toString();
        String otp4 = et_otp4.getText().toString();

        if (otp1.isEmpty() || otp2.isEmpty() || otp3.isEmpty() || otp4.isEmpty()) {
            Toast.makeText(this, "OTP is invalid", Toast.LENGTH_SHORT).show();
        } else {
            String otpFull = otp1 + otp2 + otp3 + otp4;
            if (otpFull.matches(otp)) {
                startActivity(new Intent(VerifyOTPActivity.this, WhatsBirthdayActivity.class));
            } else {
                Render render=new Render(VerifyOTPActivity.this);
                render.setDuration(900);
                render.setAnimation(Attention.Shake(ll_otp));
                render.start();
                Toast.makeText(this, "OTP does not match.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void textwatcher() {
        et_otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    et_otp2.requestFocus();
                }
            }
        });

        et_otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    et_otp3.requestFocus();
                } else if (s.length() == 0) {
                    et_otp1.requestFocus();
                }
            }
        });

        et_otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    et_otp4.requestFocus();
                } else if (s.length() == 0) {
                    et_otp2.requestFocus();
                }
            }
        });

        et_otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    et_otp3.requestFocus();
                }

                if (!et_otp1.getText().toString().isEmpty() && !et_otp2.getText().toString().isEmpty() && !et_otp3.getText().toString().isEmpty() && !et_otp4.getText().toString().isEmpty()) {
                    matchOTP();
                }
            }
        });

    }
}
