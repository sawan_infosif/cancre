package com.cancre.cancre.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cancre.cancre.activity.videoEditor.activity.MyVideoEditorActivity;
import com.cancre.cancre.adapters.AdapterNotificationVideo;
import com.cancre.cancre.models.ModelVideoList;
import com.cancre.cancre.models.NotificationVideoModel;
import com.danikula.videocache.HttpProxyCacheServer;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheKeyFactory;
import com.google.android.exoplayer2.upstream.cache.CacheUtil;
import com.google.android.exoplayer2.util.Util;
import com.cancre.cancre.R;
import com.cancre.cancre.adapters.AdapterHashTagVideoPlayer;
import com.cancre.cancre.adapters.AdapterLikedVideoPlayer;
import com.cancre.cancre.adapters.AdapterOtherUserVideo;
import com.cancre.cancre.adapters.AdapterSearchSoundPlayer;
import com.cancre.cancre.adapters.AdapterSearchVideoPlayer;
import com.cancre.cancre.adapters.AdapterUploadedVideos;
import com.cancre.cancre.fragments.homefrags.CommentFragment;
import com.cancre.cancre.javaClasses.ViewVideo;
import com.cancre.cancre.models.GetSoundDetailsPojo;
import com.cancre.cancre.models.ModelHashTagsDetails;
import com.cancre.cancre.models.ModelLikedVideos;
import com.cancre.cancre.models.ModelMyUploadedVideos;
import com.cancre.cancre.models.SearchAllPojo;
import com.cancre.cancre.mvvm.VideoMvvm;
import com.cancre.cancre.utils.App;
import com.cancre.cancre.utils.AppConstants;
import com.cancre.cancre.utils.CommonUtils;
import com.google.android.exoplayer2.video.VideoListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.security.AccessController.getContext;

public class SelectedVideoActivity extends AppCompatActivity implements Player.EventListener {
    private RecyclerView recycler_video_single;
    private LinearLayoutManager layoutManager;
    private Activity activity;
    private SpinKitView p_bar;
    int currentItems, totalItem, scrollOutItems, page = 0;
    int currentPage = -1;
    private ViewVideo viewVideo;
    boolean is_user_stop_video = false;
    private final int TYPE_SEARCH_VIDEO = 0;
    private final int TYPE_UPLOADED_VIDEO = 1;
    private final int TYPE_OTHER_USER_VIDEO = 2;
    private final int TYPE_SOUND_VIDEO = 3;
    private final int TYPE_HASHTAG_VIDEO = 4;
    private final int TYPE_LIKED_VIDEO = 5;
    private final int TYPE_NOTIFICATION_VIDEO = 6;
    private int TYPE = 1;
    private String userId = "0", videoId= "";
    private VideoMvvm videoMvvm;
    List<ModelMyUploadedVideos.Detail> listMyUploaded = new ArrayList<>();
    List<ModelLikedVideos.Detail> listLikedvideo = App.getSingleton().getLikedVideoList();
    List<SearchAllPojo.Details.VideoList> listSearchVideo = new ArrayList<>();
    List<GetSoundDetailsPojo.Details.SoundVideo> listSoundDetail = new ArrayList<>();
    List<ModelHashTagsDetails.Details.HashtagVideo> listHashTag = new ArrayList<>();
    List<NotificationVideoModel.Detail> listNotifiation = new ArrayList<>();
    boolean adapterAdd = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_video);
        activity = SelectedVideoActivity.this;
        videoMvvm = ViewModelProviders.of(SelectedVideoActivity.this).get(VideoMvvm.class);
        viewVideo = new ViewVideo(SelectedVideoActivity.this);
        if (App.getSharedpref().isLogin(activity)) {
            userId = CommonUtils.userId(activity);
        }
        findIds();
        getData();
    }

    private void getData() {


        if (getIntent().getExtras() != null) {
            int position = getIntent().getExtras().getInt(AppConstants.POSITION);

            switch (getIntent().getExtras().getString(AppConstants.SINGLE_VIDEO_TYPE)) {
                case AppConstants.TYPE_LIKED:
                    TYPE = TYPE_LIKED_VIDEO;
                    listLikedvideo = App.getSingleton().getLikedVideoList();
                    setRecycler(listMyUploaded, position, TYPE, listLikedvideo, listSearchVideo, listSoundDetail, listHashTag, listNotifiation);
                    break;

                case AppConstants.TYPE_UPLOADED:
                    TYPE = TYPE_UPLOADED_VIDEO;
                    listMyUploaded = App.getSingleton().getListMyUploaded();
                    setRecycler(listMyUploaded, position, TYPE, listLikedvideo, listSearchVideo, listSoundDetail, listHashTag, listNotifiation);
                    break;

                case AppConstants.OTHER_USER:
                    TYPE = TYPE_OTHER_USER_VIDEO;
                    listMyUploaded = App.getSingleton().getListMyUploaded();
                    setRecycler(listMyUploaded, position, TYPE, listLikedvideo, listSearchVideo, listSoundDetail, listHashTag, listNotifiation);
                    break;

                case AppConstants.SOUNDS:
                    TYPE = TYPE_SOUND_VIDEO;
                    listSoundDetail = App.getSingleton().getSoundDetailList();
                    setRecycler(listMyUploaded, position, TYPE, listLikedvideo, listSearchVideo, listSoundDetail, listHashTag, listNotifiation);
                    break;

                case AppConstants.HASHTAG:
                    TYPE = TYPE_HASHTAG_VIDEO;
                    listHashTag = App.getSingleton().getHashTagDetailList();
                    setRecycler(listMyUploaded, position, TYPE, listLikedvideo, listSearchVideo, listSoundDetail, listHashTag, listNotifiation);
                    break;

                case AppConstants.SEARCH_VIDEO:
                    TYPE = TYPE_SEARCH_VIDEO;
                    listSearchVideo = App.getSingleton().getSearchVideoList();
                    setRecycler(listMyUploaded, position, TYPE, listLikedvideo, listSearchVideo, listSoundDetail, listHashTag, listNotifiation);
                    break;

                case AppConstants.TYPE_NOTIFICATION_VIDEO:
                    TYPE = TYPE_NOTIFICATION_VIDEO;
                    videoId = getIntent().getExtras().getString("notificationVideoId");
                    notificationVideo(videoId);
//                    listNotifiation = App.getSingleton().getSearchVideoList();
                    break;
            }
            mainCachingFunction(listMyUploaded, listLikedvideo, listSearchVideo, listSoundDetail, listHashTag);
//            setRecycler(listMyUploaded, position, TYPE, listLikedvideo, listSearchVideo, listSoundDetail, listHashTag, listNotifiation);
        }
    }

    private void findIds() {
        p_bar = findViewById(R.id.p_bar);
        recycler_video_single = findViewById(R.id.recycler_video_single);
    }

    private void setRecycler(final List<ModelMyUploadedVideos.Detail> uploadedvideoList, int position, final int type
            , final List<ModelLikedVideos.Detail> listLikedvideo, final List<SearchAllPojo.Details.VideoList> listSearchVideo, final List<GetSoundDetailsPojo.Details.SoundVideo> listSoundDetail
            , final List<ModelHashTagsDetails.Details.HashtagVideo> listHashTag, final List<NotificationVideoModel.Detail> listNotifiation) {

        layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        SnapHelper snapHelper = new PagerSnapHelper();
        recycler_video_single.setItemAnimator(new DefaultItemAnimator());
        recycler_video_single.setLayoutManager(layoutManager);
        snapHelper.attachToRecyclerView(recycler_video_single);
        // this is the scroll listener of recycler view which will tell the current item number
        recycler_video_single.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //here we find the current item number
                currentItems = layoutManager.getChildCount();
                totalItem = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                final int scrollOffset = recyclerView.computeVerticalScrollOffset();
                final int height = recyclerView.getHeight();
                int page_no = scrollOffset / height;
                if (page_no != currentPage) {
                    currentPage = page_no;
                    String videoPath = "", videoId = "", ownerId = "";
                    switch (type) {
                        case TYPE_HASHTAG_VIDEO:
                            ownerId = listHashTag.get(currentPage).getUserId();
                            videoId = listHashTag.get(currentPage).getId();
                            videoPath = listHashTag.get(currentPage).getVideoPath();
                            viewVideo.viewVideoAPi(videoId);
                            break;

                        case TYPE_SEARCH_VIDEO:
                            ownerId = listSearchVideo.get(currentPage).getUserId();
                            videoId = listSearchVideo.get(currentPage).getId();
                            videoPath = listSearchVideo.get(currentPage).getVideoPath();
                            viewVideo.viewVideoAPi(videoId);
                            break;

                        case TYPE_LIKED_VIDEO:
                            ownerId = listLikedvideo.get(currentPage).getUserId();
                            videoId = listLikedvideo.get(currentPage).getId();
                            viewVideo.viewVideoAPi(videoId);
                            videoPath = listLikedvideo.get(currentPage).getVideoPath();
                            break;

                        case TYPE_UPLOADED_VIDEO:
                            ownerId = uploadedvideoList.get(currentPage).getUserId();
                            videoId = uploadedvideoList.get(currentPage).getId();
                            videoPath = uploadedvideoList.get(currentPage).getVideoPath();
                            break;

                        case TYPE_OTHER_USER_VIDEO:
                            ownerId = uploadedvideoList.get(currentPage).getUserId();
                            videoId = uploadedvideoList.get(currentPage).getId();
                            viewVideo.viewVideoAPi(uploadedvideoList.get(currentPage).getId());
                            videoPath = uploadedvideoList.get(currentPage).getVideoPath();
                            break;


                        case TYPE_SOUND_VIDEO:
                            ownerId = listSoundDetail.get(currentPage).getUserId();
                            videoId = listSoundDetail.get(currentPage).getId();
                            viewVideo.viewVideoAPi(videoId);
                            videoPath = listSoundDetail.get(currentPage).getVideoPath();
                            break;

                        case TYPE_NOTIFICATION_VIDEO:
                            ownerId = listNotifiation.get(currentPage).getUserId();
                            videoId = listNotifiation.get(currentPage).getId();
                            viewVideo.viewVideoAPi(videoId);
                            videoPath = listNotifiation.get(currentPage).getVideoPath();
                            break;

                        default:
                            videoPath = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4";
                    }
                    Release_Privious_Player();
                    Set_Player(videoPath, videoId, ownerId);
                    Log.i("scroolll", "current:" + currentPage);
                    Log.i("scroolll", "page:" + page_no);
                }
            }
        });

        recyclerAdapters(type, position, uploadedvideoList, listLikedvideo, listSearchVideo, listSoundDetail, listHashTag, listNotifiation);

    }

    private void recyclerAdapters(int type, int position, final List<ModelMyUploadedVideos.Detail> uploadedvideoList, final List<ModelLikedVideos.Detail> listLikedvideo, final List<SearchAllPojo.Details.VideoList> listSearchVideo, final List<GetSoundDetailsPojo.Details.SoundVideo> listSoundDetail
            , final List<ModelHashTagsDetails.Details.HashtagVideo> listHashTag, final List<NotificationVideoModel.Detail> listNotification) {
        switch (type) {
            case TYPE_HASHTAG_VIDEO:
                AdapterHashTagVideoPlayer adapterHashTagVideoPlayer = new AdapterHashTagVideoPlayer(activity, listHashTag, new AdapterHashTagVideoPlayer.Select() {
                    @Override
                    public void comments(int position) {
                        boolean allowcomments = ((listHashTag.get(position).getAllowComment()).equalsIgnoreCase("1")) ? true : false;
                        CommentFragment commentFragment = new CommentFragment(listHashTag.get(position).getId(), allowcomments, userId, position);
                        commentFragment.show(getSupportFragmentManager(), commentFragment.getTag());
                    }

                    @Override
                    public void share(int position) {
                        CommonUtils.shareVideoDownload(activity, listHashTag.get(position).getDownloadPath());
                    }

                    @Override
                    public void sounds(int position) {

                        App.getSingleton().setSoundId(listHashTag.get(position).getSoundId());
                        startActivity(new Intent(activity, SoundVideoActivity.class));
                    }

                    @Override
                    public void duetLayout(int position) {
                        Intent intent = new Intent(SelectedVideoActivity.this, MyVideoEditorActivity.class);
                        intent.putExtra("video_duet", uploadedvideoList.get(position).getVideoPath());
                        startActivity(intent);

                    }

                    @Override
                    public void likeVideo(int position) {
                        likeUnlike(listHashTag.get(position).getId(), listHashTag.get(position).getUserId(), position, 0);
                    }

                    @Override
                    public void hashTags(String id) {
//                        String[] hashTagId = TextUtils.split(listHashTag.get(position).getHashtag(), ",");
                        moveToHashTag(id);
                    }

                    @Override
                    public void moveToProfile(int position) {
                        moveToUserProfile(listHashTag.get(position).getUserId());
                    }
                });
                if (adapterAdd) {
                    adapterAdd = false;
                    recycler_video_single.setAdapter(adapterHashTagVideoPlayer);
                    recycler_video_single.scrollToPosition(position);
                }


                break;

            case TYPE_SEARCH_VIDEO:
                AdapterSearchVideoPlayer adapterSearchVideoPlayer = new AdapterSearchVideoPlayer(activity, listSearchVideo, new AdapterSearchVideoPlayer.Select() {
                    @Override
                    public void comments(int position) {
                        boolean allowcomments = ((listSearchVideo.get(position).getAllowComment()).equalsIgnoreCase("1")) ? true : false;
                        CommentFragment commentFragment = new CommentFragment(listSearchVideo.get(position).getId(), allowcomments, userId, position);
                        commentFragment.show(getSupportFragmentManager(), commentFragment.getTag());
                    }

                    @Override
                    public void share(int position) {
                        CommonUtils.shareVideoDownload(activity, listSearchVideo.get(position).getDownloadPath());
                    }

                    @Override
                    public void sounds(int position) {
                        //TODO


                        App.getSingleton().setSoundId(listSearchVideo.get(position).getSoundTitle());
                        startActivity(new Intent(activity, SoundVideoActivity.class));
                    }

                    @Override
                    public void duetLayout(int position) {
                        //TODO

                        Intent intent = new Intent(SelectedVideoActivity.this, MyVideoEditorActivity.class);
                        intent.putExtra("video_duet", listSearchVideo.get(position).getVideoPath());
                        startActivity(intent);
                    }

                    @Override
                    public void likeVideo(int position) {
                        likeUnlike(listSearchVideo.get(position).getId(), listSearchVideo.get(position).getUserId(), position, 0);
                    }

                    @Override
                    public void hashTags(int position) {
                        String[] hashTagId = TextUtils.split(listSearchVideo.get(position).getHashTag(), ",");
                        moveToHashTag(hashTagId[0]);
                    }

                    @Override
                    public void moveToProfile(int position) {
                        moveToUserProfile(listSearchVideo.get(position).getUserId());
                    }
                });
                if (adapterAdd) {
                    adapterAdd = false;
                    recycler_video_single.setAdapter(adapterSearchVideoPlayer);
                    recycler_video_single.scrollToPosition(position);
                }
                break;


            case TYPE_SOUND_VIDEO:

                AdapterSearchSoundPlayer adapterSearchSoundPlayer = new AdapterSearchSoundPlayer(activity, listSoundDetail, new AdapterSearchSoundPlayer.Select() {
                    @Override
                    public void comments(int position) {
                        boolean allowcomments = ((listSoundDetail.get(position).getAllowComment()).equalsIgnoreCase("1")) ? true : false;
                        CommentFragment commentFragment = new CommentFragment(listSoundDetail.get(position).getId(), allowcomments, userId, position);
                        commentFragment.show(getSupportFragmentManager(), commentFragment.getTag());
                    }

                    @Override
                    public void share(int position) {
                        CommonUtils.shareVideoDownload(activity, listSoundDetail.get(position).getDownloadPath());
                    }

                    @Override
                    public void sounds(int position) {
                        App.getSingleton().setSoundId(listSoundDetail.get(position).getSoundId());
                        startActivity(new Intent(activity, SoundVideoActivity.class));
                    }

                    @Override
                    public void duetLayout(int position) {
                        Intent intent = new Intent(SelectedVideoActivity.this, MyVideoEditorActivity.class);
                        intent.putExtra("video_duet", listSoundDetail.get(position).getVideoPath());
                        startActivity(intent);
                    }

                    @Override
                    public void likeVideo(int position) {
                        likeUnlike(listSoundDetail.get(position).getId(), listSoundDetail.get(position).getUserId(), position, 0);
                    }

                    @Override
                    public void hashTags(int position) {
                        String[] hashTagId = TextUtils.split(listSoundDetail.get(position).getHashTag(), ",");
                        moveToHashTag(hashTagId[0]);
                    }

                    @Override
                    public void moveToProfile(int position) {
                        moveToUserProfile(listSoundDetail.get(position).getUserId());
                    }
                });
                if (adapterAdd) {
                    adapterAdd = false;
                    recycler_video_single.setAdapter(adapterSearchSoundPlayer);
                    recycler_video_single.scrollToPosition(position);
                }
                break;


            case TYPE_OTHER_USER_VIDEO:
                AdapterOtherUserVideo adapterOtherUserVideo = new AdapterOtherUserVideo(activity, uploadedvideoList, new AdapterOtherUserVideo.Select() {
                    @Override
                    public void comments(int position) {
                        boolean allowcomments = ((uploadedvideoList.get(position).getAllowComment()).equalsIgnoreCase("1")) ? true : false;
                        CommentFragment commentFragment = new CommentFragment(uploadedvideoList.get(position).getId(), allowcomments, userId, position);
                        commentFragment.show(getSupportFragmentManager(), commentFragment.getTag());
                    }

                    @Override
                    public void share(int position) {
                        CommonUtils.shareVideoDownload(activity, uploadedvideoList.get(position).getDownloadPath());
                    }

                    @Override
                    public void sounds(int position) {
                        App.getSingleton().setSoundId(uploadedvideoList.get(position).getSoundId());
                        startActivity(new Intent(activity, SoundVideoActivity.class));
                    }

                    @Override
                    public void duetLayout(int position) {
                        Intent intent = new Intent(SelectedVideoActivity.this, MyVideoEditorActivity.class);
                        intent.putExtra("video_duet", uploadedvideoList.get(position).getVideoPath());
                        startActivity(intent);
                    }

                    @Override
                    public void likeVideo(int position) {
                        likeUnlike(uploadedvideoList.get(position).getId(), uploadedvideoList.get(position).getUserId(), position, 0);
                    }

                    @Override
                    public void hashTags(String id) {
//                        String[] hashTagId = TextUtils.split(uploadedvideoList.get(position).getHashtag(), ",");
                        moveToHashTag(id);
                    }

                    @Override
                    public void moveToProfile(int position) {
                        moveToUserProfile(uploadedvideoList.get(position).getUserId());
                    }
                });
                if (adapterAdd) {
                    adapterAdd = false;
                    recycler_video_single.setAdapter(adapterOtherUserVideo);
                    recycler_video_single.scrollToPosition(position);
                }
                break;

            case TYPE_LIKED_VIDEO:
                AdapterLikedVideoPlayer adapterLikedVideoPlayer = new AdapterLikedVideoPlayer(activity, listLikedvideo, new AdapterLikedVideoPlayer.Select() {
                    @Override
                    public void likeVideo(int position) {
                        likeUnlike(listLikedvideo.get(position).getVideoId(), listLikedvideo.get(position).getOwnerId(), position, 0);
                    }

                    @Override
                    public void share(int position) {
                        CommonUtils.shareVideoDownload(activity, listLikedvideo.get(position).getDownloadPath());
                    }

                    @Override
                    public void comments(int position) {
                        boolean allowcomments = ((listLikedvideo.get(position).getAllowComment()).equalsIgnoreCase("1")) ? true : false;
                        CommentFragment commentFragment = new CommentFragment(listLikedvideo.get(position).getId(), allowcomments, userId, position);
                        commentFragment.show(getSupportFragmentManager(), commentFragment.getTag());
                    }

                    @Override
                    public void sounds(int position) {
                        App.getSingleton().setSoundId(listLikedvideo.get(position).getSoundId());
                        startActivity(new Intent(activity, SoundVideoActivity.class));
                    }

                    @Override
                    public void duetLayout(int position) {
                        Intent intent = new Intent(SelectedVideoActivity.this, MyVideoEditorActivity.class);
                        intent.putExtra("video_duet", listLikedvideo.get(position).getVideoPath());
                        startActivity(intent);
                    }

                    @Override
                    public void hashTags(int position) {
                        String[] hashTagId = TextUtils.split(listLikedvideo.get(position).getHashTag(), ",");
                        moveToHashTag(hashTagId[0]);
                    }

                    @Override
                    public void moveToProfile(int position) {
                        moveToUserProfile(listLikedvideo.get(position).getUserId());
                    }
                });
                if (adapterAdd) {
                    adapterAdd = false;
                    recycler_video_single.setAdapter(adapterLikedVideoPlayer);
                    recycler_video_single.scrollToPosition(position);
                }
                break;


            case TYPE_NOTIFICATION_VIDEO:
                AdapterNotificationVideo adapterNotificationVideo = new AdapterNotificationVideo(activity, listNotification, new AdapterNotificationVideo.Select() {
                    @Override
                    public void comments(int position) {
                        boolean allowcomments = ((listNotification.get(position).getAllowComment()).equalsIgnoreCase("1")) ? true : false;
                        CommentFragment commentFragment = new CommentFragment(listNotification.get(position).getId(), allowcomments, userId, position);
                        commentFragment.show(getSupportFragmentManager(), commentFragment.getTag());
                    }

                    @Override
                    public void share(int position) {
                        CommonUtils.shareVideoDownload(activity, listNotification.get(position).getDownloadPath());
                    }

                    @Override
                    public void sounds(int position) {
                        App.getSingleton().setSoundId(listNotification.get(position).getSoundId());
                        startActivity(new Intent(activity, SoundVideoActivity.class));
                    }

                    @Override
                    public void duetLayout(int position) {
                        Intent intent = new Intent(SelectedVideoActivity.this, MyVideoEditorActivity.class);
                        intent.putExtra("video_duet", listNotification.get(position).getVideoPath());
                        startActivity(intent);
                    }

                    @Override
                    public void likeVideo(int position) {
                        likeUnlike(listNotification.get(position).getId(), listNotification.get(position).getUserId(), position, 0);
                    }

                    @Override
                    public void hashTags(String id) {
//                        String[] hashTagId = TextUtils.split(uploadedvideoList.get(position).getHashtag(), ",");
                        moveToHashTag(id);
                    }

                    @Override
                    public void moveToProfile(int position) {
                        moveToUserProfile(listNotification.get(position).getUserId());
                    }
                });
                if (adapterAdd) {
                    adapterAdd = false;
                    recycler_video_single.setAdapter(adapterNotificationVideo);
                    recycler_video_single.scrollToPosition(position);
                }
                break;



            case TYPE_UPLOADED_VIDEO:
                AdapterUploadedVideos adapterUploadedVideos = new AdapterUploadedVideos(activity, uploadedvideoList, new AdapterUploadedVideos.Select() {
                    @Override
                    public void comments(int position) {
                        boolean allowcomments = ((uploadedvideoList.get(position).getAllowComment()).equalsIgnoreCase("1")) ? true : false;
                        CommentFragment commentFragment = new CommentFragment(uploadedvideoList.get(position).getId(), allowcomments, userId, position);
                        commentFragment.show(getSupportFragmentManager(), commentFragment.getTag());
                    }

                    @Override
                    public void share(int position) {
                        CommonUtils.shareVideoDownload(activity, uploadedvideoList.get(position).getDownloadPath());
                    }

                    @Override
                    public void sounds(int position) {
                        App.getSingleton().setSoundId(uploadedvideoList.get(position).getSoundId());
                        startActivity(new Intent(activity, SoundVideoActivity.class));
                    }

                    @Override
                    public void duetLayout(int position) {
                        Intent intent = new Intent(SelectedVideoActivity.this, MyVideoEditorActivity.class);
                        intent.putExtra("video_duet", uploadedvideoList.get(position).getVideoPath());
                        startActivity(intent);
                    }

                    @Override
                    public void likeVideo(int position) {
                        likeUnlike(uploadedvideoList.get(position).getId(), userId, position, 0);
                    }

                    @Override
                    public void hashTags(String id) {
//                        String[] hashTagId = TextUtils.split(uploadedvideoList.get(position).getHashtag(), ",");
                        moveToHashTag(id);
                    }

                    @Override
                    public void moveToProfile(int position) {
                        moveToUserProfile(uploadedvideoList.get(position).getUserId());
                    }
                });
                if (adapterAdd) {
                    adapterAdd = false;
                    recycler_video_single.setAdapter(adapterUploadedVideos);
                    recycler_video_single.scrollToPosition(position);
                }

                break;

        }
    }

    private void moveToUserProfile(String videoOwnerId) {
        if (videoOwnerId.equalsIgnoreCase(userId)) {
            startActivity(new Intent(SelectedVideoActivity.this, HomeActivity.class).putExtra(AppConstants.LOGIN_TYPE, AppConstants.LOGIN_PROFILE));
        } else {
            startActivity(new Intent(SelectedVideoActivity.this, OtherUserProfileActivity.class).putExtra(AppConstants.OTHER_USER_ID, videoOwnerId));
            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        }

    }

    private void moveToHashTag(String hashTagId) {
        startActivity(new Intent(SelectedVideoActivity.this, HashTagVideoActivity.class).putExtra(AppConstants.HASHTAG_ID, hashTagId));

//        Toast.makeText(activity, "Link to page Under development.." + hashTagId, Toast.LENGTH_SHORT).show();
    }

    public void Set_Player(String videoPath, final String videoId, final String ownerId) {
        try {

            DefaultTrackSelector trackSelector = new DefaultTrackSelector();
            HttpProxyCacheServer proxy = App.getProxy(getApplicationContext());
            String proxyUrl = proxy.getProxyUrl(videoPath);
            final SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(SelectedVideoActivity.this, trackSelector);
            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(SelectedVideoActivity.this,
                    Util.getUserAgent(SelectedVideoActivity.this, "CanCre"));
            MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(Uri.parse(proxyUrl));

            player.prepare(videoSource);
            player.setRepeatMode(Player.REPEAT_MODE_ALL);
            player.addListener(this);

            View layout = layoutManager.findViewByPosition(currentPage);

            final PlayerView playerView = layout.findViewById(R.id.playerview);
            playerView.setPlayer(player);

            player.setPlayWhenReady(true);
            privious_player = player;
            player.addVideoListener(new VideoListener() {
                @Override
                public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
                    if (pixelWidthHeightRatio > 1.1 || height < width) {
                        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                    } else {
                        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);

                    }
                }

                @Override
                public void onRenderedFirstFrame() {

                }
            });

            final RelativeLayout mainlayout = layout.findViewById(R.id.rl_main_video);
            playerView.setOnTouchListener(new View.OnTouchListener() {
                private GestureDetector gestureDetector = new GestureDetector(SelectedVideoActivity.this, new GestureDetector.SimpleOnGestureListener() {

                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                        super.onFling(e1, e2, velocityX, velocityY);
                        float deltaX = e1.getX() - e2.getX();
                        float deltaXAbs = Math.abs(deltaX);
                        if ((deltaXAbs > 100) && (deltaXAbs < 1000)) {
                            if (deltaX > 0) {
                            }
                        }
                        return true;
                    }

                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        super.onSingleTapUp(e);
                        if (!player.getPlayWhenReady()) {
                            is_user_stop_video = false;
                            privious_player.setPlayWhenReady(true);
                        } else {
                            is_user_stop_video = true;
                            privious_player.setPlayWhenReady(false);
                        }

                        return true;
                    }

                    @Override
                    public void onLongPress(MotionEvent e) {
                        super.onLongPress(e);

                    }

                    @Override
                    public boolean onDoubleTap(MotionEvent e) {
                        if (!player.getPlayWhenReady()) {
                            is_user_stop_video = false;
                            privious_player.setPlayWhenReady(true);
                        }

                        if (App.getSharedpref().isLogin(SelectedVideoActivity.this)) {
                            Show_heart_on_DoubleTap(mainlayout, e);

                            likeUnlike(videoId, ownerId, currentPage, 1);

                        } else {
                            Toast.makeText(SelectedVideoActivity.this, "Please Login into app", Toast.LENGTH_SHORT).show();
                        }
                        return super.onDoubleTap(e);
                    }
                });

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    gestureDetector.onTouchEvent(event);
                    return true;
                }
            });
            playerView.setVisibility(View.VISIBLE);

            ImageView soundimage = (ImageView) layout.findViewById(R.id.sound_image_layout);
            Animation sound_animation = AnimationUtils.loadAnimation(SelectedVideoActivity.this, R.anim.d_clockwise_rotation);
            soundimage.startAnimation(sound_animation);
        } catch (Exception e) {

        }

    }

    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == Player.STATE_BUFFERING) {
            p_bar.setVisibility(View.VISIBLE);
        } else if (playbackState == Player.STATE_READY) {
            p_bar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    SimpleExoPlayer privious_player;

    public void Release_Privious_Player() {
        if (privious_player != null) {
            privious_player.removeListener(this);
            privious_player.release();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Release_Privious_Player();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        Release_Privious_Player();
        if (privious_player != null) {
            privious_player.setPlayWhenReady(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
//        Release_Privious_Player();
        if (privious_player != null) {
            privious_player.setPlayWhenReady(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Release_Privious_Player();
        if (privious_player != null) {
            privious_player.release();
        }
    }

    private void likeUnlike(String videoId, String ownerId, final int position, final int type) {
        String userid = CommonUtils.userId(activity);
        videoMvvm.likeVideo(activity, userid, videoId, ownerId).observe(SelectedVideoActivity.this, new Observer<Map>() {
            @Override
            public void onChanged(Map map) {
                if (map.get("success").equals("1")) {
                    Intent intent = new Intent(AppConstants.TYPE_LIKED);
                    intent.putExtra(AppConstants.LIKE_COUNT, map.get("like_count").toString());
                    intent.putExtra(AppConstants.POSITION, position);
                    intent.putExtra("type", type);
                    LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                } else {
                    Toast.makeText(activity, map.get("message").toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void Show_heart_on_DoubleTap(final RelativeLayout mainlayout, MotionEvent e) {

        int x = (int) e.getX() - 100;
        int y = (int) e.getY() - 100;
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        final ImageView iv = new ImageView(getApplicationContext());
        lp.setMargins(x, y, 0, 0);
        iv.setLayoutParams(lp);
        iv.setImageDrawable(getResources().getDrawable(
                R.drawable.heart_on));

        mainlayout.addView(iv);
        Animation fadeoutani = AnimationUtils.loadAnimation(activity, R.anim.fade_out);

        fadeoutani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mainlayout.removeView(iv);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        iv.startAnimation(fadeoutani);

    }

    public void mainCachingFunction(List<ModelMyUploadedVideos.Detail> listMyUploaded
            , List<ModelLikedVideos.Detail> listLikedvideo
            , List<SearchAllPojo.Details.VideoList> listSearchVideo
            , List<GetSoundDetailsPojo.Details.SoundVideo> listSoundDetail
            , List<ModelHashTagsDetails.Details.HashtagVideo> listHashTag) {
        if (canCacheData) {
            switch (getIntent().getExtras().getString(AppConstants.SINGLE_VIDEO_TYPE)) {
                case AppConstants.TYPE_LIKED:
                    for (int i = 0; i < listLikedvideo.size(); i++) {
                        String[] array = new String[]{listLikedvideo.get(i).getVideoPath(), String.valueOf(listLikedvideo.size())};
                        new CachingDataStart().execute(array);
                    }
                    break;

                case AppConstants.OTHER_USER:
                case AppConstants.TYPE_UPLOADED:
                    for (int i = 0; i < listMyUploaded.size(); i++) {
                        String[] array = new String[]{listMyUploaded.get(i).getVideoPath(), String.valueOf(listMyUploaded.size())};
                        new CachingDataStart().execute(array);
                    }
                    break;


                case AppConstants.SOUNDS:
                    for (int i = 0; i < listSoundDetail.size(); i++) {
                        String[] array = new String[]{listSoundDetail.get(i).getVideoPath(), String.valueOf(listSoundDetail.size())};
                        new CachingDataStart().execute(array);
                    }
                    break;

                case AppConstants.HASHTAG:
                    for (int i = 0; i < listHashTag.size(); i++) {
                        String[] array = new String[]{listHashTag.get(i).getVideoPath(), String.valueOf(listHashTag.size())};
                        new CachingDataStart().execute(array);
                    }
                    break;

                case AppConstants.SEARCH_VIDEO:
                    for (int i = 0; i < listSearchVideo.size(); i++) {
                        String[] array = new String[]{listSearchVideo.get(i).getVideoPath(), String.valueOf(listSearchVideo.size())};
                        new CachingDataStart().execute(array);
                    }
                    break;
            }
        }
    }

    private class CachingDataStart extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            if (Integer.parseInt(params[1]) != 0) {
                while (isCachingdata) {

                }
                if (currentCachePage + 1 < Integer.parseInt(params[1])) {
                    if (currentCachePage + 1 < currentPage) {
                        currentCachePage = currentPage;

                    } else {
                        currentCachePage += 1;

                    }
                    preCacheVideo(CachingMain(params[0]));
                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    public boolean canCacheData = true;

    public void preCacheVideo(String videoUrl) {
        Uri videoUri = Uri.parse(videoUrl);
        DataSpec dataSpec = new DataSpec(videoUri);
        CacheKeyFactory defaultCacheKeyFactory = CacheUtil.DEFAULT_CACHE_KEY_FACTORY;
        CacheUtil.ProgressListener progressListener = new CacheUtil.ProgressListener() {
            @Override
            public void onProgress(long requestLength, long bytesCached, long newBytesCached) {
                final Double downloadPercentage = bytesCached * 100.0 / requestLength;

                isCachingdata = downloadPercentage < 99;

            }
        };
        if (getApplicationContext() != null && getContext() != null) {
            DataSource dataSource = new DefaultDataSourceFactory(getApplicationContext(), Util.getUserAgent(getApplicationContext(), getString(R.string.app_name))).createDataSource();
            new CachingDataBack().execute(dataSpec, defaultCacheKeyFactory, dataSource, progressListener);
        }
    }

    public String CachingMain(String video_url) {
        HttpProxyCacheServer proxy = App.getProxy(getApplicationContext());
        String videoproxy = proxy.getProxyUrl(video_url);
        return videoproxy;
    }

    private int currentCachePage = -1;
    private boolean isCachingdata;

    private class CachingDataBack extends AsyncTask<Object, Void, Void> {

        @Override
        protected Void doInBackground(Object... params) {
            DataSpec dataSpec = (DataSpec) params[0];
            CacheKeyFactory defaultCacheKeyFactory = (CacheKeyFactory) params[1];
            DataSource dataSource = (DataSource) params[2];

            CacheUtil.ProgressListener progressListener = (CacheUtil.ProgressListener) params[3];
            try {
                CacheUtil.cache(dataSpec, App.simpleCache, defaultCacheKeyFactory, dataSource, progressListener, null);
            } catch (IOException e) {
            } catch (InterruptedException e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapterAdd = true;
        if (currentPage == 0) {
            currentPage = -1;
            if ((privious_player != null && (!is_user_stop_video))) {
                privious_player.setPlayWhenReady(true);
            }
        } else {
            if ((privious_player != null && (!is_user_stop_video))) {
                privious_player.setPlayWhenReady(true);
            }
        }
    }

    private void notificationVideo(String videoId){
        videoMvvm.singleVideo(activity, userId, videoId).observe(SelectedVideoActivity.this, new Observer<NotificationVideoModel>() {
            @Override
            public void onChanged(NotificationVideoModel notificationVideoModel) {
                if (notificationVideoModel.getSuccess().equalsIgnoreCase("1")) {
                    listNotifiation = notificationVideoModel.getDetails();
                    setRecycler(listMyUploaded, 0, TYPE, listLikedvideo, listSearchVideo, listSoundDetail, listHashTag, listNotifiation);
                }
            }
        });
    }
}