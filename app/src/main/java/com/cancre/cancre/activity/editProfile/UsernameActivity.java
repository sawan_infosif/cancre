package com.cancre.cancre.activity.editProfile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.cancre.cancre.R;
import com.cancre.cancre.javaClasses.UpdateProfileApi;
import com.cancre.cancre.models.ModelLoginRegister;
import com.cancre.cancre.utils.App;
import com.cancre.cancre.utils.AppConstants;
import com.cancre.cancre.utils.CommonUtils;

public class UsernameActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText et_username;
    String username = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonUtils.changeLanguage(UsernameActivity.this);
        setContentView(R.layout.activity_username);
        findIds();
        checkusername();
    }

    private void checkusername() {
        String username = App.getSharedpref().getModel(AppConstants.REGISTER_LOGIN_DATA, ModelLoginRegister.class).getDetails().getUsername();
        if (username.contains("@")) {
            et_username.setText(charRemoveAt(username, 0));
        } else {
            et_username.setText(username);
        }
    }

    public static String charRemoveAt(String str, int p) {
        return str.substring(0, p) + str.substring(p + 1);
    }

    private void findIds() {
        findViewById(R.id.tv_save_username).setOnClickListener(this);
        et_username = findViewById(R.id.et_username);
    }

    public void backPress(View view) {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_save_username:
                username = et_username.getText().toString();
                if (username.contains("@")) {
                    username = charRemoveAt(username, 0);
                }
                if (username.equalsIgnoreCase("") || username.isEmpty()) {
                    et_username.setError("Username cannot be empty");
                } else {
                    UpdateProfileApi updateProfileApi = new UpdateProfileApi(UsernameActivity.this);
                    updateProfileApi.hitUpdateProfile("", username, "");
                }
                break;
        }
    }
}
