package com.cancre.cancre.adapters;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cancre.cancre.R;
import com.cancre.cancre.models.ModelNotifications;
import com.cancre.cancre.utils.AppConstants;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterNotificationsChild extends RecyclerView.Adapter<AdapterNotificationsChild.ViewHolder> {

    Context context;
    List<ModelNotifications.Detail.Listdetail> listdetails;
    Select select;

    public interface Select {
        void chooseNotification(int position);

        void followUser(int position);

        void unfollowUser(int position);

        void commentUser(int position);

        void videoPlay(int position, String videoId);
    }

    public AdapterNotificationsChild(Context context, List<ModelNotifications.Detail.Listdetail> listdetails, Select select) {
        this.context = context;
        this.listdetails = listdetails;
        this.select = select;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_notifications_child, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        LocalBroadcastManager.getInstance(context).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getExtras().getString(AppConstants.NOTI_FOLLOW) != null) {
                    if (intent.getExtras().getString(AppConstants.NOTI_FOLLOW).equalsIgnoreCase(AppConstants.FOLLOW)) {
                        if (holder.getAdapterPosition() == intent.getExtras().getInt(AppConstants.POSITION)) {
                            holder.btn_unfollow_noti.setVisibility(View.VISIBLE);
                            holder.btn_follow_signin.setVisibility(View.GONE);
                        }
                    } else if (intent.getExtras().getString(AppConstants.NOTI_FOLLOW).equalsIgnoreCase(AppConstants.UNFOLLOW)) {
                        if (holder.getAdapterPosition() == intent.getExtras().getInt(AppConstants.POSITION)) {

                            holder.btn_unfollow_noti.setVisibility(View.GONE);
                            holder.btn_follow_signin.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        }, new IntentFilter(AppConstants.FOLLOW_UNFOLLOW));


        if (listdetails.get(position).getType().equalsIgnoreCase(AppConstants.NOTI_FOLLOW)) {
            holder.ll_follow_notifications.setVisibility(View.VISIBLE);

            if (listdetails.get(position).getFollowStatus()) {
                holder.btn_follow_signin.setVisibility(View.GONE);
                holder.btn_unfollow_noti.setVisibility(View.VISIBLE);
            } else if (!listdetails.get(position).getFollowStatus()) {
                holder.btn_follow_signin.setVisibility(View.VISIBLE);
                holder.btn_unfollow_noti.setVisibility(View.GONE);
            }
            Log.i("imageNoti", listdetails.get(position).getImage() + " : " + position);
            Glide.with(context).load(listdetails.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.ic_user1)).into(holder.img_follow_noti);
            holder.tv_follow_data.setText(listdetails.get(position).getMessage() + "." + "\n" + listdetails.get(position).getTime());

            holder.btn_follow_signin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    select.followUser(position);
                }
            });

            holder.btn_unfollow_noti.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    select.unfollowUser(position);
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    select.chooseNotification(position);
                }
            });
        } else if (listdetails.get(position).getType().equalsIgnoreCase(AppConstants.NOTI_LIKE)) {
            holder.ll_like_notification.setVisibility(View.VISIBLE);
            Glide.with(context).load(listdetails.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.ic_user)).into(holder.img_like_noti);
            Glide.with(context).load(listdetails.get(position).getVideo()).placeholder(context.getResources().getDrawable(R.drawable.ic_user1)).into(holder.img_like_video);
            holder.tv_like_data.setText(listdetails.get(position).getMessage() + "\n" + listdetails.get(position).getTime());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    select.chooseNotification(position);
                }
            });
        } else if (listdetails.get(position).getType().equalsIgnoreCase(AppConstants.NOTI_COMMENT)) {
            holder.ll_comment_notification.setVisibility(View.VISIBLE);
            Glide.with(context).load(listdetails.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.poat)).into(holder.img_comment_noti);
            Glide.with(context).load(listdetails.get(position).getVideo()).placeholder(context.getResources().getDrawable(R.drawable.poat)).into(holder.img_comment_video);
            holder.tv_comment_data.setText(listdetails.get(position).getMessage() + "\n" + listdetails.get(position).getTime());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    select.commentUser(position);
                    select.videoPlay(position, listdetails.get(position).getVideoId());
                }
            });

            holder.img_comment_video.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    select.videoPlay(position, listdetails.get(position).getVideoId());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listdetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout ll_like_notification, ll_follow_notifications, ll_comment_notification;
        private TextView tv_like_data, tv_follow_data, tv_comment_data;
        private CircleImageView img_like_noti, img_follow_noti, img_comment_noti;
        private ImageView img_like_video, img_comment_video;
        private Button btn_follow_signin, btn_unfollow_noti;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            btn_unfollow_noti = itemView.findViewById(R.id.btn_unfollow_noti);
            btn_follow_signin = itemView.findViewById(R.id.btn_follow_signin);
            img_follow_noti = itemView.findViewById(R.id.img_follow_noti);
            tv_follow_data = itemView.findViewById(R.id.tv_follow_data);
            tv_follow_data = itemView.findViewById(R.id.tv_follow_data);
            tv_comment_data = itemView.findViewById(R.id.tv_comment_data);
            img_like_video = itemView.findViewById(R.id.img_like_video);
            img_comment_video = itemView.findViewById(R.id.img_comment_video);
            img_like_noti = itemView.findViewById(R.id.img_like_noti);
            img_comment_noti = itemView.findViewById(R.id.img_comment_noti);
            tv_like_data = itemView.findViewById(R.id.tv_like_data);
            ll_follow_notifications = itemView.findViewById(R.id.ll_follow_notifications);
            ll_like_notification = itemView.findViewById(R.id.ll_like_notification);
            ll_comment_notification = itemView.findViewById(R.id.ll_comment_notification);
        }
    }
}