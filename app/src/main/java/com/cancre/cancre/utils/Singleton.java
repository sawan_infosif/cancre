package com.cancre.cancre.utils;

import com.cancre.cancre.models.GetSoundDetailsPojo;
import com.cancre.cancre.models.ModelHashTagsDetails;
import com.cancre.cancre.models.ModelLikedVideos;
import com.cancre.cancre.models.ModelMyUploadedVideos;
import com.cancre.cancre.models.SearchAllPojo;

import java.util.List;

public class Singleton {

    List<ModelLikedVideos.Detail> likedVideoList;
    List<ModelMyUploadedVideos.Detail> listMyUploaded;
    List<SearchAllPojo.Details.VideoList> searchVideoList;
    List<GetSoundDetailsPojo.Details.SoundVideo> soundDetailList;
    List<ModelHashTagsDetails.Details.HashtagVideo> hashTagDetailList;
    List<String> blockedUserList;
    String soundName, soundId, idType, myId, otherUserId, emailPhone, RegisterType, password, notificationVideoId;
    static int loginType;
    boolean isGalleryVideo=false;

    public boolean isGalleryVideo() {
        return isGalleryVideo;
    }

    public void setGalleryVideo(boolean galleryVideo) {
        isGalleryVideo = galleryVideo;
    }

    public List<String> getBlockedUserList() {
        return blockedUserList;
    }

    public void setBlockedUserList(List<String> blockedUserList) {
        this.blockedUserList = blockedUserList;
    }

    public int getLoginType() {
        return loginType;
    }

    public void setLoginType(int loginType) {
        this.loginType = loginType;
    }

    public List<ModelHashTagsDetails.Details.HashtagVideo> getHashTagDetailList() {
        return hashTagDetailList;
    }

    public void setHashTagDetailList(List<ModelHashTagsDetails.Details.HashtagVideo> hashTagDetailList) {
        this.hashTagDetailList = hashTagDetailList;
    }

    public List<GetSoundDetailsPojo.Details.SoundVideo> getSoundDetailList() {
        return soundDetailList;
    }

    public void setSoundDetailList(List<GetSoundDetailsPojo.Details.SoundVideo> soundDetailList) {
        this.soundDetailList = soundDetailList;
    }

    public List<SearchAllPojo.Details.VideoList> getSearchVideoList() {
        return searchVideoList;
    }

    public void setSearchVideoList(List<SearchAllPojo.Details.VideoList> searchVideoList) {
        this.searchVideoList = searchVideoList;
    }

    public String getSoundName() {
        return soundName;
    }

    public void setSoundName(String soundName) {
        this.soundName = soundName;
    }

    public String getSoundId() {
        return soundId;
    }

    public void setSoundId(String soundId) {
        this.soundId = soundId;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getMyId() {
        return myId;
    }

    public void setMyId(String myId) {
        this.myId = myId;
    }

    public String getOtherUserId() {
        return otherUserId;
    }

    public void setOtherUserId(String otherUserId) {
        this.otherUserId = otherUserId;
    }

    public List<ModelMyUploadedVideos.Detail> getListMyUploaded() {
        return listMyUploaded;
    }

    public void setListMyUploaded(List<ModelMyUploadedVideos.Detail> listMyUploaded) {
        this.listMyUploaded = listMyUploaded;
    }

    public List<ModelLikedVideos.Detail> getLikedVideoList() {
        return likedVideoList;
    }

    public void setLikedVideoList(List<ModelLikedVideos.Detail> likedVideoList) {
        this.likedVideoList = likedVideoList;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailPhone() {
        return emailPhone;
    }

    public void setEmailPhone(String emailPhone) {
        this.emailPhone = emailPhone;
    }

    public String getRegisterType() {
        return RegisterType;
    }

    public void setRegisterType(String registerType) {
        RegisterType = registerType;
    }

    public String getNotificationVideoId() {
        return notificationVideoId;
    }

    public void setNotificationVideoId(String notificationVideoId) {
        this.notificationVideoId = notificationVideoId;
    }
}
